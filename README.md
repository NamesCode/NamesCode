# Hallo 🍕

How you found this profile is unkown but here it is!
I try to contribute regularly. So far I haven't done much in the realms of Open-Source.

I'm a member of [Catppuccin](https://github.com/orgs/catppuccin/people?query=Name) :3

[![@name's Holopin board](https://holopin.me/name)](https://holopin.io/@name)

## Socials

-   Twitter: [@NameIsALoser](https://twitter.com/NameIsALoser)
-   Email: <namesexistsinemails@gmail.com>
-   Github: [NamesCode](https://github.com/NamesCode)
-   Discord: NameExists#3898

## GeekCode
```
-----BEGIN GEEK CODE BLOCK-----
Version: 3.1
GCS/E/M/S/O d-(--)>---pu s+(+)>+:-()> a--- C++(+++)>+++$ UC*()>+$ P()> L(+)>++$ E+(+)>++$ !W+>$ !N-? !o? K- !w--(--)>-- !O M()$>+ !V? PS+(+)@>+ PE()> Y+(+)>++ PGP++(+)>+++ !t-- !5- X++(++)>++ R++(++)>++$ tv()> b++(++)>+++ DI D+(+)>+ G()> e-(-)>++ h+(+)>++ r y--(--)>++
------END GEEK CODE BLOCK------ 
```
## Languages

-   [X] Python *Everyone starts somewhere
-   [X] Rust *Not finished but semi-confident*
-   [ ] ELisp *Kinda started but wouldn't count*
-   [ ] Javascript :) *WebUI's with sveltekit <3*
-   [ ] Java :( *School*

# Proj

-   [ ] [Catppuccin MDBook](https://github.com/catppuccin/mdBook)
-   [ ] Quiplash clone *Started*
-   [ ] Tauri app to run a model of cancer growth for FMSPW *Started*
-   [ ] Personal website

# TODO

-   [ ] Make personal website *Started*
-   [ ] [Learn Rust](https://doc.rust-lang.org/rust-by-example/index.html) *Started and going well :)* 

